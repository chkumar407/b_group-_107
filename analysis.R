
# loading data ------------------------------------------------------------
library(tidyverse)

dm <- read.csv("TobaccoUsageDataset.csv", as.is=T)

#Include only Men and Women related records and exclude Overall.
df <- filter(dm, Gender != "Overall" & Data_Value > 0)

names(df) <- c("YEAR","LocationAbbr","LocationDesc","TopicType","TopicDesc","MeasureDesc","DataSource","Response","Data_Value_Unit","Data_Value_Type",
               "TobaccoConsumption","Data_Value_Footnote_Symbol","Data_Value_Footnote", "Data_Value_Std_Err","Low_Confidence_Limit", "High_Confidence_Limit",
			   "Sample_Size", "Gender",	"Race",	"Age",	"Education",	"GeoLocation",	"TopicTypeId",	"TopicId",	"MeasureId",	"StratificationID1",
			   "StratificationID2",	"StratificationID3",	"StratificationID4")



# testing the means using wilcox test due to non normal data  ------------------------------------------
# mean_test <- t.test(TobaccoConsumptionValue ~ Gender,data=df)
mean_test <- wilcox.test(TobaccoConsumption~Gender,data=df)
mean_test


